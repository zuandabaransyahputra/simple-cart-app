const express = require('express')
const dotenv = require('dotenv').config()
const { errorHandler } = require("./middleware/errorMiddleware")
const connectDB = require("./config/db")


//Connec to database
connectDB()

const app = express()

//body parser
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

//routes
app.use('/api/users', require('./routes/authRoutes'))
app.use('/api/products', require('./routes/productRoutes'))

//Error handler
app.use(errorHandler)

//Port
const port = process.env.PORT || 5000

//listen application
app.listen(port, () => {
    console.log(`Running application on port: ${port}`)
})