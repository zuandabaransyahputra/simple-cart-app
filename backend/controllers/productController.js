const Product = require('../models/productModel')
const asyncHandler = require('express-async-handler')
const path = require('path')

const uploadProduct = asyncHandler(async (req, res) => {
    if (!req.body.title || !req.body.price || !req.file) {
        res.status(400)
        throw new Error('Please complete all fields')
    }
    const product = {
        title: req.body.title,
        price: req.body.price,
    }
    saveProductImage(product, req.body.product)

    await Product.create(product, (err) => {
        if (err) {
            console.log(err);
        }
    });

    res.status(200).json(product)
})

const getProduct = asyncHandler(async (req, res) => {
    try {
        const products = await Product.find({ user: req.user.id })
        res.status(200).json(products)
    } catch (error) {
        console.log(error)
        res.status(400)
        throw new Error(error)
    }
})

function saveProductImage(book, productEncoded) {
    if (productEncoded == null) return
    const product = JSON.parse(productEncoded)
    if (product != null && imageMimeTypes.includes(product.type)) {
        book.productImage = new Buffer.from(product.data, 'base64')
        book.productImageType = product.type
    }
}

module.exports = {
    uploadProduct,
    getProduct
}