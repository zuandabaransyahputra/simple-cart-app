const User = require('../models/userModel')
const asyncHandler = require('express-async-handler')
const jwt = require("jsonwebtoken")
const bcrypt = require('bcryptjs')

//Register controller
//Endpoint: api/users/
const registerUser = asyncHandler(async (req, res) => {
    const { name, email, password, role } = req.body

    //Validation fields
    if (!name || !email || !password || !role) {
        res.status(400)
        throw new Error('Please add all fields')
    }

    //Check if user exist
    const userExists = await User.findOne({ email })
    if (userExists) {
        res.status(400)
        throw new Error('User already exist')
    }

    //Hash password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(password, salt)

    //Create User
    const user = await User.create({
        name,
        email,
        role,
        password: hashedPassword
    })

    if (user) {
        res.status(201).json({
            _id: user.id,
            name: user.name,
            email: user.email,
            role: user.role
        })
    } else {
        res.status(400)
        throw new Error('Invalid user data')
    }
})

//login controller
//Endpoint: api/users/login
const loginUser = asyncHandler(async (req, res) => {
    const { email, password } = req.body

    //Check for user email
    const user = await User.findOne({ email })

    if (user && (await bcrypt.compare(password, user.password))) {
        res.json({
            _id: user.id,
            name: user.name,
            email: user.email,
            role: user.role,
            token: generateToken(user.id)
        })
    } else {
        res.status(400)
        throw new Error('Invalid credentials')
    }
})

//Get user
//Endpoint: api/users/me
const getMe = asyncHandler(async (req, res) => {
    res.status(200).json(req.user) //req.user langsung dapat dari middleware
})

//Generate JWT
const generateToken = (id) => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: '30d',
    })
}

module.exports = {
    registerUser,
    loginUser,
    getMe
}
