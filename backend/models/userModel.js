const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please input your name"]
    },
    email: {
        type: String,
        required: [true, "Please input an email"],
        unique: true
    },
    password: {
        type: String,
        required: [true, "Please add an password"]
    },
    role: {
        type: String,
        enum: ['user', 'costumer'],
        default: 'user',
        required: true
    }
},
    {
        timestamps: true
    })


module.exports = mongoose.model('User', userSchema)