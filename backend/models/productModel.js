const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    title: {
        type: String,
        required: [true, 'Please add product title']
    },
    productImage: {
        type: Buffer,
        required: true
    },
    productImageType: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: [true, 'Please add product price']
    }
}, {
    timestamps: true
})

productSchema.virtual('productImagepath').get(function () {
    if (this.productImage != null && this.productImageType != null) {
        return `data:${this.productImageType};charset=utf-8;base64,${this.productImage.toString('base64')}`
    }
})

module.exports = mongoose.model('Product', productSchema)