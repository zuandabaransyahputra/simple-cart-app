const express = require('express')
const { protect } = require('../middleware/authMiddleware')
const upload = require('../middleware/uploadMiddleware')
const { uploadProduct } = require('../controllers/productController')

const router = express.Router()

router.post('/upload', protect, upload, uploadProduct);

module.exports = router